package no.systek.gcm.test;

import android.test.ActivityInstrumentationTestCase2;
import android.test.UiThreadTest;
import android.widget.Button;
import no.systek.gcm.GCMMainActivity;
import no.systek.gcm.R;

public class GCMMainActivityTest extends ActivityInstrumentationTestCase2<GCMMainActivity> {

    public GCMMainActivityTest() {
        super(GCMMainActivity.class);
    }

    public void testActivity() {
        GCMMainActivity activity = getActivity();
        assertNotNull(activity);
    }

    @UiThreadTest
    public void testPushRegButton() {
        GCMMainActivity activity = getActivity();
        Button button = (Button) activity.findViewById(R.id.regButton);
        button.performClick();
    }
}

