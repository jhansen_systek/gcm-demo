package no.systek.gcm.test.registration;

import android.test.AndroidTestCase;
import no.systek.gcm.registration.GCMRegistrationIdSender;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 2:35 PM - 2/7/13
 */
public class GCMRegistrationIdSenderTest extends AndroidTestCase {

    public void testSendRegistrationId() {
        new GCMRegistrationIdSender().execute("test-regId");
    }
}
