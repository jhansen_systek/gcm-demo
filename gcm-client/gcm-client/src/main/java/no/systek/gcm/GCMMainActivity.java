package no.systek.gcm;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import de.akquinet.android.androlog.Log;
import no.systek.gcm.event.GCMEvent;
import no.systek.gcm.event.OnRegisteredEvent;
import no.systek.gcm.event.OnUnregisteredEvent;
import no.systek.gcm.registration.GCMRegistrationHandler;
import no.systek.gcm.registration.RegistrationHandler;

public class GCMMainActivity extends Activity {
    private RegistrationHandler registrationHandler;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.init();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        registrationHandler = new GCMRegistrationHandler(this);

        final Activity activity = this;
        registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                GCMEvent event = (GCMEvent) intent.getSerializableExtra(GCMDemoConstants.GCM_TYPE);
                event.execute(activity, registrationHandler);
            }
        }, new IntentFilter(GCMDemoConstants.GCM_DEMO_ACTION));

        createButtonView();
        createListView();
    }

    private void createButtonView() {
        if (registrationHandler.isRegistered())
            new OnRegisteredEvent().execute(this, registrationHandler);
        else
            new OnUnregisteredEvent().execute(this, registrationHandler);
    }

    private void createListView() {
        ArrayAdapter<String> messages = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);
        ListView messagesList = (ListView) findViewById(R.id.messagesList);
        messagesList.setAdapter(messages);
    }
}
