package no.systek.gcm.event;

import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import no.systek.gcm.R;
import no.systek.gcm.registration.RegistrationHandler;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 8:54 AM - 2/7/13
 */
public class OnRegisteredEvent implements GCMEvent {

    @Override
    public void execute(Activity activity, final RegistrationHandler registrationHandler) {
        Toast.makeText(activity, "Device registered", Toast.LENGTH_SHORT).show();
        final Button regButton = (Button) activity.findViewById(R.id.regButton);
        regButton.setText("Unregister device");
        regButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                registrationHandler.unregister();
            }
        });
    }
}
