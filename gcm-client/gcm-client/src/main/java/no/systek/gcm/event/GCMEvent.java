package no.systek.gcm.event;

import android.app.Activity;
import no.systek.gcm.registration.RegistrationHandler;

import java.io.Serializable;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 8:54 AM - 2/7/13
 */
public interface GCMEvent extends Serializable {
    public void execute(Activity activity, RegistrationHandler registrationHandler);
}
