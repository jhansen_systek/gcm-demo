package no.systek.gcm.registration;

import android.content.Context;
import com.google.android.gcm.GCMRegistrar;
import de.akquinet.android.androlog.Log;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 11:04 AM - 2/6/13
 */
public class GCMRegistrationHandler implements RegistrationHandler {
    private static final String PROJECT_ID = "";

    private final Context context;
    private String regId = "";

    public GCMRegistrationHandler(Context context) {
        GCMRegistrar.checkDevice(context);
        GCMRegistrar.checkManifest(context);

        this.context = context;
    }

    @Override
    public boolean isRegistered() {
        regId = GCMRegistrar.getRegistrationId(context);
        boolean registered = false;

        if (regId != null && !"".equals(regId)) {
            registered = true;
            new GCMRegistrationIdSender().execute(regId);

            Log.i("Already registered");
        }

        return registered;
    }

    @Override
    public void register() {
        GCMRegistrar.register(context, PROJECT_ID);
        regId = GCMRegistrar.getRegistrationId(context);
    }

    @Override
    public void unregister() {
        GCMRegistrar.unregister(context);
    }
}
