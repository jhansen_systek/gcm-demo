package no.systek.gcm;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 12:08 PM - 2/6/13
 */
public enum GCMDemoConstants {
    ;

    public static final String GCM_DEMO_ACTION = "no.systek.gcm.GCM_DEMO_ACTION";
    public static final String GCM_TYPE = "GCM_TYPE";
}
