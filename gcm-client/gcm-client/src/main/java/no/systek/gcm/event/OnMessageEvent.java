package no.systek.gcm.event;

import android.app.Activity;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import no.systek.gcm.R;
import no.systek.gcm.registration.RegistrationHandler;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 9:23 AM - 2/7/13
 */
public class OnMessageEvent implements GCMEvent {
    private final String message;

    public OnMessageEvent(String message) {
        this.message = message;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void execute(Activity activity, RegistrationHandler registrationHandler) {
        ListView messages = (ListView) activity.findViewById(R.id.messagesList);
        ArrayAdapter<String> adapter = (ArrayAdapter) messages.getAdapter();
        adapter.add(message + "\n");
        adapter.notifyDataSetChanged();
    }
}
