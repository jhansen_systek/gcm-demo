package no.systek.gcm.registration;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 11:06 AM - 2/6/13
 */
public interface RegistrationHandler {
    public boolean isRegistered();

    public void register();

    public void unregister();
}
