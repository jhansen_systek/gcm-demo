package no.systek.gcm.registration;

import android.os.AsyncTask;
import de.akquinet.android.androlog.Log;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 9:58 AM - 2/4/13
 */
public class GCMRegistrationIdSender extends AsyncTask<String, Void, Void> {
    private static final String REGID_URL = "http://10.0.2.2:8080/reg";

    public GCMRegistrationIdSender() {
    }

    @Override
    protected Void doInBackground(String... regIds) {
        if (regIds.length == 1) {
            Log.i("Sending regId to server");

            StringBuilder requestUrl = new StringBuilder();
            requestUrl.append(REGID_URL).append("?regId=").append(regIds[0]);

            HttpURLConnection connection = null;
            try {
                URL url = new URL(requestUrl.toString());
                connection = (HttpURLConnection) url.openConnection();
                connection.setDoOutput(false);
                connection.setDoInput(false);
                connection.setRequestMethod("GET");
                connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=UTF-8");

                int responseCode = connection.getResponseCode();
                Log.i("Response code: " + responseCode);
            } catch (MalformedURLException mal) {
                Log.e(this, "Invalid URL specified: " + requestUrl.toString(), mal);
            } catch (IOException io) {
                Log.e(this, "Unable to send registration request", io);
            } finally {
                if (connection != null)
                    connection.disconnect();
            }
        } else {
            Log.e("Error sending regId request, invalid number of parameters: " + Arrays.toString(regIds));
        }

        return null;
    }
}
