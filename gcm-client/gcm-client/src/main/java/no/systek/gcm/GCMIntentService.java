package no.systek.gcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import com.google.android.gcm.GCMBaseIntentService;
import de.akquinet.android.androlog.Log;
import no.systek.gcm.event.OnMessageEvent;
import no.systek.gcm.event.OnRegisteredEvent;
import no.systek.gcm.event.OnUnregisteredEvent;
import no.systek.gcm.registration.GCMRegistrationIdSender;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 1:10 PM - 2/1/13
 */
public class GCMIntentService extends GCMBaseIntentService {
    private static final AtomicInteger notificationCounter = new AtomicInteger(0);

    @Override
    protected void onMessage(final Context context, Intent intent) {
        Log.i("Message from project: " + intent.getStringExtra("from"));

        if (intent.hasExtra("msg")) {
            String msg = intent.getStringExtra("msg");
            sendNotification(context, msg);

            Intent localIntent = new Intent(GCMDemoConstants.GCM_DEMO_ACTION);
            OnMessageEvent event = new OnMessageEvent(msg);
            localIntent.putExtra(GCMDemoConstants.GCM_TYPE, event);
            context.sendBroadcast(localIntent);
        }
    }

    private void sendNotification(Context context, String msg) {
        int notificationId = notificationCounter.incrementAndGet();
        Intent notificationIntent = new Intent(context, GCMMainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, notificationId, notificationIntent, PendingIntent.FLAG_ONE_SHOT);

        String title = "Push msg #" + notificationId;
        Log.i("Title : " + title);
        Notification notification = new Notification.Builder(this)
                .setContentTitle(title)
                .setContentText(msg)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentIntent(pIntent)
                .setAutoCancel(true)
                .build();
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(notificationId, notification);
    }

    @Override
    protected void onError(Context context, String msg) {
        Log.i("onError(), msg: " + msg);
    }

    @Override
    protected void onRegistered(final Context context, String regId) {
        Log.i("onRegistered(), regId: " + regId);
        new GCMRegistrationIdSender().execute(regId);

        Intent intent = new Intent(GCMDemoConstants.GCM_DEMO_ACTION);
        OnRegisteredEvent event = new OnRegisteredEvent();
        intent.putExtra(GCMDemoConstants.GCM_TYPE, event);
        context.sendBroadcast(intent);
    }

    @Override
    protected void onUnregistered(final Context context, String regId) {
        Log.i("onUnregistered(), regId " + regId);

        Intent intent = new Intent(GCMDemoConstants.GCM_DEMO_ACTION);
        OnUnregisteredEvent event = new OnUnregisteredEvent();
        intent.putExtra(GCMDemoConstants.GCM_TYPE, event);
        context.sendBroadcast(intent);
    }
}
