package no.systek.gcm;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 1:30 PM - 2/5/13
 */
public enum PushMessaging {
    INSTANCE;

    private final Map<String, String> connectedDevices = new ConcurrentHashMap<String, String>();
    private final List<String> messages = new ArrayList<String>();

    private PushMessaging() {
        messages.add("Rules of Optimization: Rule 1: Don't do it. Rule 2 (for experts only): Don't do it yet.");
        messages.add("Java is to JavaScript what Car is to Carpet.");
        messages.add("Walking on water and developing software from a specification are easy if both are frozen.");
        messages.add("Always code as if the guy who ends up maintaining your code will be a violent psychopath who knows where you live.");
        messages.add("Any fool can write code that a computer can understand. Good programmers write code that humans can understand.");
        messages.add("Programs must be written for people to read, and only incidentally for machines to execute.");
        messages.add("Copy and paste is a design error.");
        messages.add("Before software can be reusable it first has to be usable.");
        messages.add("Computers are good at following instructions, but not at reading your mind.");
        messages.add("It's hard enough to find an error in your code when you're looking for it; it's even harder when you've assumed your code is error-free.");
        messages.add("A good programmer is someone who looks both ways before crossing a one-way street.");
        messages.add("Hey! It compiles! Ship it!");
        messages.add("I don't care if it works on your machine! We are not shipping your machine!");
        messages.add("First learn computer science and all the theory. Next develop a programming style. Then forget all that and just hack.");
        messages.add("Exception up = new Exception('Something is really wrong.');\nthrow up;  //ha ha");
        messages.add("// somedev1 -  6/7/02 Adding temporary tracking of Login screen\n// somedev2 -  5/22/07 Temporary my ass");
        messages.add("// I am not sure if we need this, but too scared to delete.");
        messages.add("// Mr. Compiler, please do not read this.");
        messages.add("If debugging is the process of removing software bugs, then programming must be the process of putting them in.");
        messages.add("Measuring programming progress by lines of code is like measuring aircraft building progress by weight.");
    }

    public void addRegId(String regId, String userAgent) {
        connectedDevices.put(regId, userAgent);
    }

    public Map<String, String> getConnectedDevices() {
        return connectedDevices;
    }

    public String getMessage() {
        Random random = new Random();
        int index = random.nextInt(messages.size());
        return messages.get(index);
    }
}
