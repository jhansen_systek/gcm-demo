package no.systek.gcm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 9:43 AM - 2/4/13
 */
public class RegistrationServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(RegistrationServlet.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String regId = request.getParameter("regId");

        if (regId != null && !"".equals(regId)) {
            String userAgent = request.getHeader("User-Agent");
            PushMessaging.INSTANCE.addRegId(regId, userAgent);
            logger.info("User-Agent: {}", userAgent);
            logger.info("Received regId: {}", regId);
        } else
            logger.warn("No registration id received");
    }
}
