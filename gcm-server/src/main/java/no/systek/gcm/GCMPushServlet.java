package no.systek.gcm;

import com.google.android.gcm.server.Message;
import com.google.android.gcm.server.MulticastResult;
import com.google.android.gcm.server.Sender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @Author Jarle Hansen (jarle@jarlehansen.net)
 * Created: 1:28 PM - 2/5/13
 */
public class GCMPushServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(GCMPushServlet.class);
    private static final String API_KEY = "";

    private final AtomicInteger counter = new AtomicInteger(0);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        PrintWriter writer = response.getWriter();

        Map<String, String> connectedDevices = PushMessaging.INSTANCE.getConnectedDevices();

        if (connectedDevices.isEmpty()) {
            writer.print("<!DOCTYPE html><html><head><META HTTP-EQUIV='refresh' CONTENT='3'></head><body><img src='images/waiting.jpg'/><h2>Waiting for devices to register</h2></body></html>");
        } else {
            int count = counter.incrementAndGet();

            String msg = PushMessaging.INSTANCE.getMessage();
            writer.print("<!DOCTYPE html><html><head><META HTTP-EQUIV='refresh' CONTENT='10'></head><body><img src='images/message.png'/>" +
                    "<h2>Device(s) registered: </h2><ul>");

            for (Map.Entry<String, String> entry : connectedDevices.entrySet()) {
                writer.println("<li><strong>User-Agent: </strong>" + entry.getValue() + "</li>");
                writer.println("<li><strong>RegId: </strong>" + entry.getKey() + "</li>");
            }

            writer.println("</ul><h2>Sending message number " + count + ":</h2><h3>" + msg + "</h3></body></html>");

            Sender sender = new Sender(API_KEY);
            Message message = new Message.Builder().addData("msg", msg).build();
            Set<String> regIds = connectedDevices.keySet();
            MulticastResult result = sender.send(message, new ArrayList<String>(regIds), 0);
            logger.info(result.toString());

            int failureId = result.getFailure();
            if (failureId > 0) {
                logger.error("Problem sending message, failure id: " + failureId);
            }
        }
    }
}
